package com.groupon.game.chess.exception;

public class ConcurrentMoveNotAllowedException extends RuntimeException {
    public ConcurrentMoveNotAllowedException(String reason) {
        super(reason);
    }
}
