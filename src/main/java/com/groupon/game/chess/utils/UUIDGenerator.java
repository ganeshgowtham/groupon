package com.groupon.game.chess.utils;

import java.io.Serializable;
import java.util.UUID;

/**
 * Creates the UUID used for creating gameId
 */
public class UUIDGenerator implements Serializable {
    private static UUIDGenerator currentInstance;

    private UUIDGenerator() {
    }

    public static UUIDGenerator getInstance() {
        if (currentInstance == null) {
            synchronized (UUIDGenerator.class) {
                if (currentInstance == null) {
                    //Lazy init
                    currentInstance = new UUIDGenerator();
                }
            }
        }
        return currentInstance;
    }

    public String getUniqueId() {
        return UUID.randomUUID().toString();
    }

}

