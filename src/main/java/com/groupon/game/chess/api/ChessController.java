package com.groupon.game.chess.api;

import com.groupon.game.chess.models.api.ChessGame;
import com.groupon.game.chess.models.api.StartGame;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "chessgame/v1", consumes = "application/json", produces = "application/json")
public class ChessController {

    /*
    @Autowired
    Inject ValidationService & GamingService
     */

    @PostMapping(path = "/start")
    public ChessGame startGame(@RequestBody StartGame startGame) {
        return new ChessGame();
    }

    @DeleteMapping(path = "end/{gameId}")
    public boolean endGame(@PathVariable String gameId) {
        return true;
    }

    @PostMapping(path = "/move/{gameId}")
    public boolean move(@PathVariable String gameId) {
        return true;
    }

    @PostMapping(path = "/signal/{gameId}")
    public boolean signal(@PathVariable String gameId) {
        return true;
    }

    @PostMapping(path = "/surrender/{gameId}")
    public boolean surrender(@PathVariable String gameId) {
        return true;
    }
}
