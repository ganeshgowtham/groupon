package com.groupon.game.chess.models.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChessGame {
    private String uuid;
    private String gameName;
    private Date startTime;
    private Date endTime;
    private List<Player> players = null;

    public ChessGame() {
        players = new ArrayList<>(2);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "ChessGame{" +
                "uuid='" + uuid + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
