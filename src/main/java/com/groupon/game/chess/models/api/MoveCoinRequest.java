package com.groupon.game.chess.models.api;

import com.groupon.game.chess.models.CoinType;

public class MoveCoinRequest {
    private CoinType type;
    private CoinPosition fromPosition;
    private CoinPosition toPosition;

    public CoinType getType() {
        return type;
    }

    public void setType(CoinType type) {
        this.type = type;
    }

    public CoinPosition getFromPosition() {
        return fromPosition;
    }

    public void setFromPosition(CoinPosition fromPosition) {
        this.fromPosition = fromPosition;
    }

    public CoinPosition getToPosition() {
        return toPosition;
    }

    public void setToPosition(CoinPosition toPosition) {
        this.toPosition = toPosition;
    }
}
