package com.groupon.game.chess.models;

public enum CoinType {
    KING, ROOKS, PAWNS, BISHOPS, QUEEN
}
