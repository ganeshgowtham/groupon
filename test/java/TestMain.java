import org.springframework.util.StopWatch;

public class TestMain {
    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch("cool");
        stopWatch.start("s1");
        stopWatch.stop();
        stopWatch.start("s2");
        stopWatch.stop();
        stopWatch.shortSummary();
        System.out.println(stopWatch.prettyPrint());
    }
}
